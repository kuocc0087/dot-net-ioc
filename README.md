﻿﻿# .NET IoC (Inversion of Control) and DI (Dependency Injection) in non-ASP.NET Core application

First off, it is not entirely necessary to use a Dependency Injection (DI) service  in your application. The overhead and complexity may not be worth the trouble and performance impact. On the other hand, it can be a nice addition to your application and may add clarity to your code and dependencies. I had added DI in the flavor of what is provided by default in a new ASP.NET Core project to a regular .NET Standard library which was used in a Windows Service and a .NET Core Console app. This project shows what I had to do to get it working.

Inversion of Control and Dependency Injection are patterns probably best described by the many [blogs](https://www.c-sharpcorner.com/UploadFile/cda5ba/dependency-injection-di-and-inversion-of-control-ioc/) out on the internet. Using Dependency Injection and Inversion of Control can help you maintain loosely coupled objects in your code. Using a DI service is useful when you create unit tests.

I used .NET Core 2.2, but I believe you should be able to use the .NET SDKs that the NuGet package Microsoft.Extensions.DependencyInjection lists as dependencies. This project is a .NET Core Console app.

## Prerequisites

- .NET Core 2.2

## Dependencies

Open the project NuGet Package Manager and install the following dependencies:

- Microsoft.Extensions.DependencyInjection

## Add the DI service

Typically, the DI service is created in the `Main` method or startup of your application.

In the `Program.Main` method, we will add the following code:

```csharp

// ...
using Microsoft.Extensions.DependencyInjection;

internal class Program 
{
    static void Main(string[] args)
    {
        var serviceProvider = new ServiceCollection()
            .BuildServiceProvider();
    }
}

```

An ASP.NET Core project creates an `IServiceCollection` instance for you and provides it as a parameter in the `Startup.ConfigureServices` method. What we have to do is create our own `IServiceCollection` object, which the Microsoft.Extensions.DependencyInjection package provides an implementation as `ServiceCollection`.

That is pretty much it! You can add your DI definitions the same way as you normally would in a ASP.NET application. But we can actually see it in action.

## Using the DI service

The following interfaces are what object types we will want the DI service to instantiate and inject into a class that will use them.

```csharp

public interface IPrompt
{
    void Prompt();
}

```

```csharp

public interface INamePrompt : IPrompt
{
}

```

```csharp
public interface IDogsPrompt : IPrompt
{
}

```

`INamePrompt` is implemented by the class `NamePrompt`.

```csharp

public class NamePrompt : INamePrompt
{
    public void Prompt()
    {
        Console.WriteLine("What is your name?");
        var name = Console.ReadLine().Trim();
        Console.WriteLine($"Nice to meet you, {name}!");
    }
}

```

`IDogsPrompt` is implemented by the class `DogsPrompt`.

```csharp

public class DogsPrompt : IDogsPrompt
{
    private readonly ImmutableDictionary<string, Action<DogsPrompt>> _responses = new Dictionary<string, Action<DogsPrompt>>
    {
        { "y", context => context.RespondForYes() },
        { "n", context => context.RespondForNo() },
        { "default", context => context.RespondForDefault() }
    }.ToImmutableDictionary();

    public void Prompt()
    {
        Console.WriteLine("Do you like dogs? (Enter Y or N)");
        var answer = Console.ReadLine().Trim().ToLower();

        var action = _responses.GetValueOrDefault(answer, _responses["default"]);
        action.Invoke(this);
    }

    private void RespondForDefault()
    {
        Console.WriteLine("Sorry, I didn't recognize that answer...");
        Prompt();
    }

    private void RespondForNo()
    {
        Console.WriteLine("Squirrel!");
    }

    private void RespondForYes()
    {
        Console.WriteLine("I like dogs, too!");
    }
}

```

You can see that `DogsPrompt` is slightly more complicated than `NamePrompt`, but it still implements `IPrompt.Prompt` and will invoke the method if invalid input had been received.

Those classes are great an all, but we need something to orchestrate the prompting that the application will be doing. A class `Prompter` will do the work.

```csharp

public class Prompter
{
    private List<IPrompt> _prompts = new List<IPrompt>();

    public Prompter(INamePrompt namePrompt, IDogsPrompt dogsPrompt)
    {
        _prompts.Add(namePrompt);
        _prompts.Add(dogsPrompt);
    }

    public void Prompt()
    {
        _prompts.ForEach(prompt =>
        {
            prompt.Prompt();
        });
        Console.WriteLine("Thank you, bye!");
    }
}

```

Things to notice is that in the constructor, we are explicitly defining that the `INamePrompt` and `IDogsPrompt` object types are dependencies that need to be injected by the user of this `Prompter` class. We add those objects to a `List<IPrompt>` defined above.

In `Prompter.Prompt`, we iterate through the `IPrompt` objects and invoke each `IPrompt.Prompt` method. Our `Prompter` merely knows that the objects stored in the list must implement the `IPrompt.Prompt` method and invokes generically.

To finally make this all work, we need to modify the `Main` method in `Program`.

```csharp

internal class Program
{
    private static void Main(string[] args)
    {
        var serviceProvider = new ServiceCollection()
            .AddSingleton<Prompter>()
            .AddScoped<INamePrompt, NamePrompt>()
            .AddScoped<IDogsPrompt, DogsPrompt>()
            .BuildServiceProvider();

        var prompter = serviceProvider.GetService<Prompter>();
        prompter.Prompt();

        Task.Delay(1000).Wait();
    }
}

```

What we have now are definitions added to the DI service by use of `AddSingleton` and `AddScoped`. For more information between the differences between the types, you can read more [here](https://docs.microsoft.com/en-us/dotnet/api/microsoft.extensions.dependencyinjection.servicelifetime?view=aspnetcore-2.2#Microsoft_Extensions_DependencyInjection_ServiceLifetime_Scoped).

Specifically looking at the `AddScoped` invocations, you can see that for the first generic type provided, the DI service will need to create an instance of the second generic type provided.

After we've defined our DI service, we can use it to get a `Prompter` instance by using its `GetService<>` method. In that call, we are allowing the DI service to give us an instance of the `Prompter` class and have the service also create instances of the dependencies defined in the `Prompter` constructor. Nested dependencies will also be injected by the DI service if defined in `NamePrompt` or `DogsPrompt`.

If we weren't to use `var prompter = serviceProvider.GetService<Prompter>()` to get our `Prompter` instance and used the `new` keyword, that would mean that it would be necessary for us to create `NamePrompt` and `DogsPrompt` instances and provided them through the constructor. The DI service allows us to define what the application should use in a central place for specific implementation, but you may code generically in most places. Where a DI service may shine is to use different implementation of a type like `IDogsPrompt` depending on the environment the application is running. When defining the DI service, we could choose to first check the environment and use a different implementation if we were in production versus test or development.

## Final notes

You may not need to use a DI service like the one Microsoft provides in Microsoft.Extensions.DependencyInjection or [Castle Windsor](http://www.castleproject.org/projects/windsor/). It certainly adds overhead because a DI service more than likely uses [Reflection](https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/concepts/reflection) while instantiating objects. Reflection can be a very expensive operation, and your application may not want to risk higher processing costs. Another issue is that those DI service definitions can be huge as a project grows larger and larger.

The Inversion of Control (IOC) and Dependency Injection (DI) patterns are still very valuable outside of using a DI service. It would still be a good idea to require your classes and methods to define their dependencies upfront rather than do magical creation of objects. Use of class factories are still very relevant with or without a DI service in your project.