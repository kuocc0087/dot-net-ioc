﻿using Microsoft.Extensions.DependencyInjection;
using System.Threading.Tasks;

namespace DotNetIoC
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var serviceProvider = new ServiceCollection()
                .AddSingleton<Prompter>()
                .AddScoped<INamePrompt, NamePrompt>()
                .AddScoped<IDogsPrompt, DogsPrompt>()
                .BuildServiceProvider();

            var prompter = serviceProvider.GetService<Prompter>();
            prompter.Prompt();

            Task.Delay(1000).Wait();
        }
    }
}