﻿namespace DotNetIoC
{
    public interface IPrompt
    {
        void Prompt();
    }
}