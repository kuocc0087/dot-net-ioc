﻿using System;

namespace DotNetIoC
{
    public class NamePrompt : INamePrompt
    {
        public void Prompt()
        {
            Console.WriteLine("What is your name?");
            var name = Console.ReadLine().Trim();
            Console.WriteLine($"Nice to meet you, {name}!");
        }
    }
}