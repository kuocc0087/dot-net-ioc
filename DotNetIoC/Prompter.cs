﻿using System;
using System.Collections.Generic;

namespace DotNetIoC
{
    public class Prompter
    {
        private List<IPrompt> _prompts = new List<IPrompt>();

        public Prompter(INamePrompt namePrompt, IDogsPrompt dogsPrompt)
        {
            _prompts.Add(namePrompt);
            _prompts.Add(dogsPrompt);
        }

        public void Prompt()
        {
            _prompts.ForEach(prompt =>
            {
                prompt.Prompt();
            });
            Console.WriteLine("Thank you, bye!");
        }
    }
}