﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;

namespace DotNetIoC
{
    public class DogsPrompt : IDogsPrompt
    {
        private readonly ImmutableDictionary<string, Action<DogsPrompt>> _responses = new Dictionary<string, Action<DogsPrompt>>
        {
            { "y", context => context.RespondForYes() },
            { "n", context => context.RespondForNo() },
            { "default", context => context.RespondForDefault() }
        }.ToImmutableDictionary();

        public void Prompt()
        {
            Console.WriteLine("Do you like dogs? (Enter Y or N)");
            var answer = Console.ReadLine().Trim().ToLower();

            var action = _responses.GetValueOrDefault(answer, _responses["default"]);
            action.Invoke(this);
        }

        private void RespondForDefault()
        {
            Console.WriteLine("Sorry, I didn't recognize that answer...");
            Prompt();
        }

        private void RespondForNo()
        {
            Console.WriteLine("Squirrel!");
        }

        private void RespondForYes()
        {
            Console.WriteLine("I like dogs, too!");
        }
    }
}